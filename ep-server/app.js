var express = require('express');
var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser());

var lastEp = 3;

app.get('/lastEp', function (req, res) {
  res.send(lastEp + "");
});

app.post('/lastEp', function (req, res){
  var newLastEp = req.body.value;
  lastEp = newLastEp;
  res.send(200);
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});
