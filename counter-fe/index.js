var plusEle = $("#bot-container button:nth-child(1)");
var minusEle = $("#bot-container button:nth-child(2)");
var numContainer = $("#top-container");

var setCount = (count) => {
    numContainer.text(count);
    $.post("http://localhost:3000/lastEp", {
        value: count
    });
}

plusEle.click(() => {
    var target = +numContainer.text() + 1;
    setCount(target);
});

minusEle.click(() => {
    var target = +numContainer.text() - 1;
    if (target < 0) return;
    setCount(target);
});

$.get("http://localhost:3000/lastEp", count => {
    numContainer.text(count);
});